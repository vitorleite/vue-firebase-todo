import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

// creating a Axios instance with a baseURL to my Firebase, just so we don't need to constantly repeat the url
const axfb = axios.create({
    baseURL: 'YOUR-FIREBASE-URL-HERE' // e.g. (this one actually works) https://vue-firebase-todo-b4099.firebaseio.com
})


const state = {
    todos: []
}

const mutations = {
  // set all todos
  setTodos (state, todos) {
    state.todos = todos;
  },

  // toggle a single item 'done' state
  toggleTodo (state, todo) {
    todo.done = !todo.done
  },

  // add a new item to the state
  addTodo (state, todo) {
    state.todos.push(todo)
  },

  // delete an item
  deleteTodo (state, index) {
    state.todos.splice(index, 1);
  }
}

const actions = {
  // load whatever we have in firebase
  loadData ({ commit }) {
    axfb.get('todos.json')
      .then(response => response.data || []) // get response.data OR an empty array (if it's null)
      .then(data => {
        commit('setTodos', data);
      })
  },

  // save to firebase, we just 'put' it there
  saveData ({ state }) {
    axfb.put('todos.json', state.todos)
  },

  // addTodo action will
  // - call the addTodo mutation
  // - call the saveData action
  addTodo ({ commit, dispatch }, todo) {
    todo.done = false; // item is always false when created
    commit('addTodo', todo)
    dispatch('saveData')
  },

  // delete removes the item and saves the data
  deleteTodo ({ commit, dispatch }, index) {
    commit('deleteTodo', index)
    dispatch('saveData')
  },

  // toggleTodo action will (similarly to adding)
  // - call the toggleTodo mutation
  // - call the saveData action
  toggleTodo ({ commit, dispatch }, todo) {
    commit('toggleTodo', todo)
    dispatch('saveData')
  },
}

export default new Vuex.Store({
  state,
  mutations,
  actions
})
